/*Lector RSVP
 Copyright (C) 2016  Edgar Pérez
 
 This file is part of Lector RSVP.
 
 Lector RSVP is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 Lector RSVP is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Lector RSVP.  If not, see <http://www.gnu.org/licenses/>.
 */

//Declarar las variables que contendrán la historia y la posición actual
var arreglo = "";
var actual = -1;
var milisegundos = 200;
var ppm = 300;

/* Funciones */
//Convierte a horas, minutos y segundos el tiempo restante aproximado de la lectura
var convertirSegundos = function convertirSegundos(s) {
    s = s / parseInt(ppm) * 60;
    var segundos = parseInt(s, 10);
    var horas = Math.floor(segundos / 3600);
    var minutos = Math.floor((segundos - (horas * 3600)) / 60);
    segundos = segundos - (horas * 3600) - (minutos * 60);

    if (horas < 10) {
        horas = "0" + horas;
    }
    if (minutos < 10) {
        minutos = "0" + minutos;
    }
    if (segundos < 10) {
        segundos = "0" + segundos;
    }
    var tiempo = horas + ':' + minutos + ':' + segundos;
    return tiempo;
}

//Actualiza la barra de progreso
var progreso = function progreso(actual, total) {
    var barra = $('#barra');
    var porcentaje = +((parseInt(actual) / parseInt(total)) * 100).toFixed(2);
    barra.val(porcentaje);
    $('#porcentajeCompleto').html(porcentaje + '%');
}
//Función llamada para pasar a la siguiente palabra
var repetir = function repetir(j = 1)
{
    var $destino = $('#texto');
    actual = localStorage.getItem("actual");
    if (parseInt(actual) >= arreglo.length)
    {
        clearInterval(Mostrar);
        $("#cargar").prop("disabled", false);
        $("#reiniciar").prop("disabled", false);
        $("#pausar").prop("disabled", true);
        localStorage.setItem("actual", arreglo.length);
        progreso(arreglo.length, arreglo.length);
                return;
    }
    $destino.empty();
    if (j === -1) {
        actual = parseInt(actual) - 1;
    } else {
        actual = parseInt(actual) + 1;
    }
    $("#restante").html(convertirSegundos((arreglo.length - actual)));
    progreso(actual, arreglo.length);
    $destino.append(arreglo[actual]);
    localStorage.setItem("actual", actual);
}
//Sigue la lectura
var continuar = function continuar() {
    milisegundos = calcularMS();
    Mostrar = setInterval(repetir, milisegundos);
}
//Calcula los milisegundos que cada palabra está en pantalla
var calcularMS = function calcularMS() {
    ppm = parseInt($('#ppm').val());
    var velocidad = 60 / ppm * 1000;
    return velocidad;
}
//Cargar datos
var cargarDatos = function cargarDatos() {
    if (localStorage.getItem("actual") === null || parseInt(localStorage.getItem("actual")) >= arreglo.length) {
        localStorage.setItem("actual", -1);
    }
    milisegundos = calcularMS();
    Mostrar = setInterval(repetir, milisegundos);
}
//Buscar archivos de texto en el directorio libro, esto funciona con el servidor Apache
var listadoLibros = function listadoLibros() {
    var opcion = "";
    $.get("libros/", function (data) {
        $(data).find("td > a[href*='.txt']").each(function () {
            opcion = decodeURIComponent($(this).attr("href"));
            if (localStorage.getItem("archivo") === opcion) {
                $("#libros").append('<option selected>' + opcion + '</option>');
            } else {
                $("#libros").append('<option>' + opcion + '</option>');
            }

        });
    }).done(function () {
        if (localStorage.getItem("arreglo") !== null && localStorage.getItem("actual") !== null) {
            $("#continuar").prop("disabled", false);
        } else if (localStorage.getItem("arreglo") !== null && localStorage.getItem("actual") === null) {
            $("#iniciar").prop("disabled", false);
        }
    });
}

//Cargar contenido del libro
var cargarLibro = function cargarLibro(libro) {
    $.get("libros/" + libro, function (data) {
        var contenido = data.split(/(\s+)/);
        var limpio = contenido.filter(function (e) {
            return e.replace(/(\s+)/gm, "")
        });
        localStorage.setItem("archivo", libro);
        localStorage.setItem("arreglo", JSON.stringify(limpio));
        arreglo = JSON.parse(localStorage["arreglo"]);
    }).done(function () {
        $("#iniciar").prop("disabled", false);
        $("#texto").html('Libro cargado.');
        progreso(0, 100);
        $("#restante").html(convertirSegundos(arreglo.length));
        milisegundos = calcularMS();
    });
}

/* Al cargar el sitio que cargue los datos por defecto si existen */

$(document).ready(function () {
    //Cargar el listado de libros en formato TXT
    listadoLibros();

    //Si existe el valor actual asignarlo a la variable
    if (localStorage.getItem("actual") !== null) {
        actual = localStorage.getItem("actual");
    }

    //Si hay un libro cargado actualmente entonces asignarlo a la variable
    if (localStorage.getItem("arreglo") !== null) {
        arreglo = JSON.parse(localStorage["arreglo"]);
        $('#texto').append(arreglo[actual]);
        progreso(actual, arreglo.length);
        $("#restante").html(convertirSegundos((arreglo.length - actual)));
    }
    //Calcular los milisegundos que se mostrará cada "palabra" en pantalla
    milisegundos = calcularMS();
});

/* Los botones */

//Pausar la lectura
$("#pausar").click(function () {
    clearInterval(Mostrar);
    $("#pausar").prop("disabled", true);
    $("#continuar, #reiniciar, #cargar, #atras, #adelante").prop("disabled", false);
});
//Continuar la lectura
$("#continuar").click(function () {
    $("#pausar").prop("disabled", false);
    $("#continuar, #reiniciar, #cargar, #atras, #adelante").prop("disabled", true);
    continuar();
});
//Reiniciar la lectura
$("#reiniciar").click(function () {
    localStorage.setItem("actual", -1);
    $("#continuar, #iniciar, #reiniciar, #cargar, #atras, #adelante").prop("disabled", true);
    $("#pausar").prop("disabled", false);
    cargarDatos();
});
//Cargar un nuevo libro
$("#cargar").click(function () {
    $("#continuar, #reiniciar").prop("disabled", true);
    localStorage.clear();
    cargarLibro($("#libros option:selected").text());
});
//Cargar nueva lectura
$("#iniciar").click(function () {
    $("#continuar, #iniciar, #reiniciar, #atras, #adelante, #cargar").prop("disabled", true);
    $("#pausar").prop("disabled", false);
    cargarDatos();
});
//Adelanta una palabra
$("#adelante").click(function () {
    if (localStorage.getItem("actual") !== null) {
        if (parseInt(actual) >= arreglo.length - 1)
        {
            $("#adelante").prop("disabled", true);
        } else {
            $("#atras").prop("disabled", false);
            repetir();
        }
    }
});
//Retrocede una palabra
$("#atras").click(function () {
    if (localStorage.getItem("actual") !== null) {
        if (parseInt(actual) <= 0)
        {
            $("#atras").prop("disabled", true);
        } else {
            $("#adelante").prop("disabled", false);
            repetir(-1);
        }
    }
});